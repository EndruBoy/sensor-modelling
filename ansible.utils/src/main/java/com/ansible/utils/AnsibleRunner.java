package com.ansible.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import exceptions.AnsibleException;


public class AnsibleRunner {
	public static String executeCommand(String command, Logger LOGGER) throws AnsibleException {

		StringBuffer output = new StringBuffer();

		Process p = null;
		try {
			p = Runtime.getRuntime().exec(command);
		} catch (IOException e) {
			throw new AnsibleException(e.getMessage());
		}
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			throw new AnsibleException(e.getMessage());
		}

		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

		String line = "";
		try {
			while ((line = reader.readLine()) != null) {
				LOGGER.log(Level.INFO, line);
				output.append(line + "\n");
			}
		} catch (IOException e) {
			throw new AnsibleException(e.getMessage());
		}
		if (p.exitValue() != 0) {
			throw new AnsibleException("Ansible command failed with exit code: " + p.exitValue() + " logs:\n" + output);
		}

		return output.toString();

	}
}