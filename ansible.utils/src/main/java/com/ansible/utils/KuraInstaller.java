package com.ansible.utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.google.common.io.Resources;

import exceptions.AnsibleException;

public class KuraInstaller {
	private final static Logger LOGGER = Logger.getLogger(KuraInstaller.class.getName());

	public static void install(List<String> ips, String brokerIp, String pubKey) throws AnsibleException {
		String inventoryFilePath = createInventoryFile(ips, brokerIp);
		executeKuraInstallAnsible(inventoryFilePath);
	}

	public static String executeKuraInstallAnsible(String inventoryFilePath) throws AnsibleException {
		String playbookPath = Resources.getResource("install-kura.yaml").getPath();
		String ansibleCommand = "ansible-playbook -vv -s " + playbookPath + " -i " + inventoryFilePath + " -u pi";
		String ansibleOutput = "";
		ansibleOutput = AnsibleRunner.executeCommand(ansibleCommand, LOGGER);
		return ansibleOutput;
	}

	public static String createInventoryFile(List<String> ips, String brokerIp) {
		LOGGER.log(Level.INFO, "Creating Inventory file for ips: " + ips.toString() + " \n\tbroker IP: " + brokerIp);
		String INVENTORY_FILE = "inventory.conf";
		Path file = Paths.get(INVENTORY_FILE);
		Charset charset = Charset.forName("UTF-8");
		try {
			Files.write(file, "[pi]\n".getBytes(charset));
			Files.write(file, ips, charset, StandardOpenOption.APPEND);
			Files.write(file, "[broker]\n".getBytes(charset), StandardOpenOption.APPEND);
			if (brokerIp == null) {
				LOGGER.info("Broker not specified, using first IP:" + ips.get(0));
				Files.write(file, ips.get(0).getBytes(charset), StandardOpenOption.APPEND);
			} else {
				for (String ip : ips) {
					if (ip.equals(brokerIp)){
						Files.write(file, ip.getBytes(charset), StandardOpenOption.APPEND);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return file.toAbsolutePath().toString();
	}
}
