package exceptions;

public class AnsibleException extends Exception {

	private static final long serialVersionUID = -5520949418562907990L;

	public AnsibleException() {
		super();
	}

	public AnsibleException(String message) {
		super(message);
	}

}