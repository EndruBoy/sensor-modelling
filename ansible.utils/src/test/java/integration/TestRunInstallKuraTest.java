package integration;

import static org.junit.Assert.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import com.ansible.utils.KuraInstaller;
import com.google.common.io.Resources;

public class TestRunInstallKuraTest {
	private final static Logger LOGGER = Logger.getLogger(TestRunInstallKuraTest.class.getName());
	private KuraInstaller runner;

	@Before
	public void setUpBefore() {
		runner = new KuraInstaller();
	}
	@Test
	public void testAnsibleCanConnect() throws Exception  {
		// given
		String InvPath = Resources.getResource("test_inventory.conf").getPath();
		// when
		LOGGER.log(Level.INFO, runner.executeKuraInstallAnsible(InvPath));
		// then no exception occurred
	}
	
	@Test
	public void testInventoryCreate() throws Exception {
		// given
		ArrayList<String> ips = new ArrayList<String>();
		String ip = "127.0.0.1 ansible_port=2222";
		String ip2 = "127.0.0.1 ansible_port=2223";
		ips.add(ip);
		ips.add(ip2);
		// when
		String pathToInventory = runner.createInventoryFile(ips, ip2);
		// then
		Path file = Paths.get(pathToInventory);
		List<String> content = Files.readAllLines(file);
		assertEquals(content.get(0), "[pi]");
		assertEquals(content.get(1), ip);
		assertEquals(content.get(2), ip2);
		assertEquals(content.get(3), "[broker]");
		assertEquals(content.get(4), ip2);
	}
}
