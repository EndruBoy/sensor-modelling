package api;


import catalog.DeploymentPackage;
import catalog.PackageLoader;
import kura.config.mqtt_call.PackageInstaller;

public class PackageManager {
	public static void installDeploymentPackage(String gatewayAddr, String dpName, String brokerUrl) {
		PackageInstaller installer = new PackageInstaller();
		DeploymentPackage dp = PackageLoader.getPackage(dpName);
		installer.install(gatewayAddr, dp, brokerUrl);
	}
}
