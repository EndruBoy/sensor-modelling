package catalog;

public class DeploymentPackage {
	private String name;
	private String uri;
	private String downloadProtocol;
	private String version;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getDownloadProtocol() {
		return downloadProtocol;
	}

	public void setDownloadProtocol(String downloadProtocol) {
		this.downloadProtocol = downloadProtocol;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
