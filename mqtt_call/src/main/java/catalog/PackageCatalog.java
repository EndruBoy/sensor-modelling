package catalog;

import java.util.List;

public class PackageCatalog {
	private List<DeploymentPackage> packages;

	public List<DeploymentPackage> getPackages() {
		return packages;
	}

	public void setPackages(List<DeploymentPackage> packages) {
		this.packages = packages;
	}

}
