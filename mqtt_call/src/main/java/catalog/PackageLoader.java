package catalog;

import java.io.IOException;
import java.io.InputStream;

import org.yaml.snakeyaml.Yaml;

import com.google.common.io.Resources;

public class PackageLoader {
	public static DeploymentPackage getPackage(String name) {
		Yaml yaml = new Yaml();  
		
        try( InputStream in = Resources.getResource("packages.yaml").openStream() ) {
        	PackageCatalog dpcatalog = yaml.loadAs( in, PackageCatalog.class );
            for (DeploymentPackage dp : dpcatalog.getPackages()) {
            	if (dp.getName().equalsIgnoreCase(name)) {
            		return dp;
            	}
            }
        } catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
}
