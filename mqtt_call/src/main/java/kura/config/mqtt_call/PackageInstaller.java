package kura.config.mqtt_call;

import com.amitinside.mqtt.client.kura.message.KuraPayload;
import com.amitinside.mqtt.client.kura.message.payload.operator.KuraPayloadEncoder;

import catalog.DeploymentPackage;

import java.io.IOException;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;

public class PackageInstaller implements MqttCallback {
	KuraPayload payload = null;
	public void install(String gatewayDomain,DeploymentPackage dp, String brokerUrl) {
		payload = new KuraPayload();
		payload.addMetric("dp.uri", dp.getUri());
		payload.addMetric("dp.name", dp.getName());
		payload.addMetric("dp.version", dp.getVersion());
		payload.addMetric("dp.download.protocol", dp.getDownloadProtocol());
		runClient(brokerUrl, gatewayDomain);
	}

	MqttClient myClient;
	MqttConnectOptions connOpt;

	static final String M2MIO_DOMAIN = "<Insert m2m.io domain here>";
	static final String M2MIO_STUFF = "things";
	static final String M2MIO_THING = "<Unique device ID>";
	static final String M2MIO_USERNAME = "<m2m.io username>";
	static final String M2MIO_PASSWORD_MD5 = "<m2m.io password (MD5 sum of password)>";

	// the following two flags control whether this example is a publisher, a
	// subscriber or both
	static final Boolean subscriber = true;
	static final Boolean publisher = true;

	/**
	 * 
	 * connectionLost This callback is invoked upon losing the MQTT connection.
	 * 
	 */
	public void connectionLost(Throwable t) {
		System.out.println("Connection lost!");
		// code to reconnect to the broker would go here if desired
	}

	/**
	 * 
	 * deliveryComplete This callback is invoked when a message published by
	 * this client is successfully received by the broker.
	 * 
	 */
	public void deliveryComplete(MqttDeliveryToken token) {
		// System.out.println("Pub complete" + new
		// String(token.getMessage().getPayload()));
	}

	/**
	 * 
	 * messageArrived This callback is invoked when a message is received on a
	 * subscribed topic.
	 * 
	 */
	public void messageArrived(MqttTopic topic, MqttMessage message) throws Exception {
		System.out.println("-------------------------------------------------");
		System.out.println("| Topic:" + topic.getName());
		System.out.println("| Message: " + new String(message.getPayload()));
		System.out.println("-------------------------------------------------");
	}

	/**
	 * 
	 * runClient The main functionality of this simple example. Create a MQTT
	 * client, connect to broker, pub/sub, disconnect.
	 * @param brokerUrl The URL of the MQTT broker
	 * 
	 * @throws IOException
	 * 
	 */
	public void runClient(String brokerUrl, String gatewayDomain) {
		// setup MQTT Client
		String clientID = M2MIO_THING;
		connOpt = new MqttConnectOptions();

		connOpt.setCleanSession(true);
		connOpt.setKeepAliveInterval(30);
		// connOpt.setUserName(M2MIO_USERNAME);
		// connOpt.setPassword(M2MIO_PASSWORD_MD5.toCharArray());

		// Connect to Broker
		try {
			myClient = new MqttClient(brokerUrl, clientID);
			myClient.setCallback(this);
			myClient.connect(connOpt);
		} catch (MqttException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		System.out.println("Connected to " + brokerUrl);

		// setup topic
		// topics on m2m.io are in the form <domain>/<stuff>/<thing>
		String myTopic = "$EDC/" + gatewayDomain + "/DEPLOY-V2/EXEC/download";
		MqttTopic topic = myClient.getTopic(myTopic);

		// subscribe to topic if subscriber
		if (subscriber) {
			try {
				int subQoS = 0;
				myClient.subscribe(myTopic, subQoS);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// publish messages if publisher
		if (publisher) {
			int pubQoS = 0;
			
			
			// Job ID, data type is long
			payload.addMetric("job.id", 12345891011L);
			// System update, data type is boolean, set to false for now
			payload.addMetric("dp.install.system.update", false);
			payload.addMetric("request.id", "REQUEST_JKPPF3LM7401LQSA319SP1USSL");
			payload.addMetric("requester.client.id", "CLIENT_2C2MSA2O6M6J2514EMO24KUI7I");
			final KuraPayloadEncoder encoder = new KuraPayloadEncoder(payload);
			MqttMessage message = null;
			try {
				message = new MqttMessage(encoder.getBytes());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			message.setQos(pubQoS);
			message.setRetained(false);

			// Publish the message
			System.out.println("Publishing to topic \"" + topic + "\" qos " + pubQoS);
			MqttDeliveryToken token = null;
			try {
				// publish message to broker
				token = topic.publish(message);
				// Wait until the message has been delivered to the broker
				token.waitForCompletion();
				Thread.sleep(100);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// disconnect
		try {
			// wait to ensure subscribed messages are delivered
			if (subscriber) {
				Thread.sleep(5000);
			}
			myClient.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO Auto-generated method stub

	}

	public void messageArrived(String arg0, MqttMessage arg1) throws Exception {
		// TODO Auto-generated method stub

	}
}
