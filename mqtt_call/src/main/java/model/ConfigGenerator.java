package model;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.yaml.snakeyaml.Yaml;

import com.ansible.utils.KuraInstaller;
import com.google.common.io.Resources;

import api.PackageManager;
import exceptions.AnsibleException;

public class ConfigGenerator {
	public static void generate() throws AnsibleException {
		Yaml yaml = new Yaml();
		IotModelConfig iotModelConfig = null;

		try (InputStream in = Resources.getResource("iot_model_config.yaml").openStream()) {
			iotModelConfig = yaml.loadAs(in, IotModelConfig.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//TODO generate Vorto info models
		
		KuraInstaller.install(getAnsibleInventory(iotModelConfig), iotModelConfig.getMqttBrokerAddress(), "");	//TODO configure pubkey
		for (Gateway gw : iotModelConfig.getGateways()) {
			for (String dpName : gw.getSensors()) {
				PackageManager.installDeploymentPackage(gw.getAddress(), dpName, iotModelConfig.getMqttBrokerAddress()); //FIXME will address be the same as domain in mqtt topic?
			}
		}
	}

	private static List<String> getAnsibleInventory(IotModelConfig iotModelConfig) {
		List<String> gwAddresses = new ArrayList<String>();
		for (Gateway gw : iotModelConfig.getGateways()) {
			gwAddresses.add(gw.getAddress());
		}
		return gwAddresses;
	}

}
