package model;

import java.util.List;

public class IotModelConfig {
	public List<Gateway> gateways;
	public String mqttBrokerAddress;

	public String getMqttBrokerAddress() {
		return mqttBrokerAddress;
	}

	public void setMqttBrokerAddress(String mqttBrokerAddress) {
		this.mqttBrokerAddress = mqttBrokerAddress;
	}

	public List<Gateway> getGateways() {
		return gateways;
	}

	public void setGateways(List<Gateway> gateways) {
		this.gateways = gateways;
	}
}
