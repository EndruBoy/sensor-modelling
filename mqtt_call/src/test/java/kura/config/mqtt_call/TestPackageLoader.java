package kura.config.mqtt_call;

import static org.junit.Assert.*;

import org.junit.Test;

import catalog.DeploymentPackage;
import catalog.PackageLoader;

public class TestPackageLoader {
	
	@Test
	public void testLoadDht11dp() throws Exception {
		// given
		String dpName = "dht11";
		// when
		DeploymentPackage dp = PackageLoader.getPackage(dpName);
		// then
		assertEquals(dp.getName(), dpName);
		assertEquals(dp.getDownloadProtocol(), "HTTP");
		assertEquals(dp.getUri(), "http://192.168.0.19:8000/dht11.dp");
		assertEquals(dp.getVersion(), "1.0.0");
		
	}
}
