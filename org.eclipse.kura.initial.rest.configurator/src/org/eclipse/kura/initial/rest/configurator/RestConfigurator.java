package org.eclipse.kura.initial.rest.configurator;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.eclipse.kura.configuration.ConfigurableComponent;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.http.HttpContext;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RestConfigurator implements ConfigurableComponent {

	private static final Logger s_logger = LoggerFactory.getLogger(RestConfigurator.class);
	private static final String SERVLET_ALIAS_ROOT = "servlet.alias.root";
	private static final String APP_ROOT = "app.root";
	
	private static ComponentContext context;
	private static String s_aliasRoot;
	private static String s_appRoot;
	//private static ComponentContext s_context;
	private static ComponentContext s_context;

	private HttpService m_httpService;
	
	//private ExecutorService m_worker;


	static ComponentContext getContext() {
		return context;
	}
	
	/*public RestConfigurator() {
		super();
		//m_worker = Executors.newSingleThreadExecutor();
	}*/
	
	public void setHttpService(HttpService httpService) {
		s_logger.info("Setting HttpService...");
		this.m_httpService = httpService;
	}

	public void unsetHttpService(HttpService httpService) {
		s_logger.info("Unsetting HttpService...");
		this.m_httpService = null;
	}
	
	public static BundleContext getBundleContext() {
		return s_context.getBundleContext();
	}

	public static String getApplicationRoot() {
		return s_appRoot;
	}

	public static String getServletRoot() {
		return s_aliasRoot;
	}

	protected void activate(ComponentContext bundleContext, Map<String, Object> properties) {
		s_logger.info("Activating...");
		RestConfigurator.context = bundleContext;
		s_aliasRoot = (String) properties.get(SERVLET_ALIAS_ROOT);
		s_appRoot = (String) properties.get(APP_ROOT);
		String servletRoot = s_aliasRoot;
		
		HttpContext httpCtx = m_httpService.createDefaultHttpContext();
		try {
			m_httpService.registerResources("/", "www", httpCtx);
			m_httpService.registerResources(s_appRoot, "www/configurator.html", httpCtx);
		} catch (NamespaceException e) {
			s_logger.error(e.getMessage());
		}
	}

	protected void deactivate(BundleContext bundleContext) {
		RestConfigurator.context = null;
		s_logger.info("deactivate...");

		//m_worker.shutdown();

		unregisterServlet();
	}
	
	protected void updated(Map<String, Object> properties) {
		
	}
	
	private void unregisterServlet() {
		m_httpService.unregister("/");
		m_httpService.unregister(s_appRoot);
		//m_httpService.unregister(s_aliasRoot);
	}

}
