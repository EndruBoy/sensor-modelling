package org.eclipse.kura.pi4j.dht11;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import com.pi4j.wiringpi.Gpio;
import com.pi4j.wiringpi.GpioUtil;


public class DHT11 {
	private static final Logger s_logger = LoggerFactory.getLogger(DHT11.class);
	private static final int MAXTIMINGS = 85;
	private int[] dht11_dat = { 0, 0, 0, 0, 0 };
	private static final int pinNum = 7;

	public DHT11() {
		s_logger.info("Setting up GPIO...");
		// setup wiringPi
		if (Gpio.wiringPiSetup() == -1) {
			s_logger.error(" ==>> GPIO SETUP FAILED");
			return;
		}
		s_logger.info("Exporting GPIO " + pinNum + "...");
		GpioUtil.export(pinNum, GpioUtil.DIRECTION_OUT);
	}

	public SensorData getTemperature() {
		int laststate = Gpio.HIGH;
		int j = 0;
		dht11_dat[0] = dht11_dat[1] = dht11_dat[2] = dht11_dat[3] = dht11_dat[4] = 0;
		StringBuilder value = new StringBuilder();

		Gpio.pinMode(pinNum, Gpio.OUTPUT);
		Gpio.digitalWrite(pinNum, Gpio.LOW);
		Gpio.delay(18);

		Gpio.digitalWrite(pinNum, Gpio.HIGH);
		Gpio.pinMode(pinNum, Gpio.INPUT);

		for (int i = 0; i < MAXTIMINGS; i++) {
			int counter = 0;
			while (Gpio.digitalRead(pinNum) == laststate) {
				counter++;
				Gpio.delayMicroseconds(1);
				if (counter == 255) {
					break;
				}
			}

			laststate = Gpio.digitalRead(pinNum);

			if (counter == 255) {
				break;
			}

			/* ignore first 3 transitions */
			if ((i >= 4) && (i % 2 == 0)) {
				/* shove each bit into the storage bytes */
				dht11_dat[j / 8] <<= 1;
				if (counter > 16) {
					dht11_dat[j / 8] |= 1;
				}
				j++;
			}
		}
		// check we read 40 bits (8bit x 5 ) + verify checksum in the last
		// byte
		if ((j >= 40) && checkParity()) {
			float h = (float) ((dht11_dat[0] << 8) + dht11_dat[1]) / 10;
			if (h > 100) {
				h = dht11_dat[0]; // for DHT11
			}
			float c = (float) (((dht11_dat[2] & 0x7F) << 8) + dht11_dat[3]) / 10;
			if (c > 125) {
				c = dht11_dat[2]; // for DHT11
			}
			if ((dht11_dat[2] & 0x80) != 0) {
				c = -c;
			}
			float f = c * 1.8f + 32;
			DHT11.s_logger.info(("Humidity = " + h + " Temperature = " + c + "(" + f + "f)"));
			SensorData data = new SensorData();
			data.setTemperature(c);
			data.setHumidity(h);
			return data;
		} else {
			DHT11.s_logger.error(("Data not good, skip"));
			return getTemperature();
		}

	}

	private boolean checkParity() {
		boolean parity = (dht11_dat[4] == ((dht11_dat[0] + dht11_dat[1] + dht11_dat[2] + dht11_dat[3]) & 0xFF));
		if (!parity)
			s_logger.error("Parity check failed");
		return parity;
	}
}