package org.eclipse.kura.pi4j.dht11;

/*******************************************************************************
 * Copyright (c) 2011, 2016 Eurotech and/or its affiliates
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Eurotech
 *******************************************************************************/

import java.util.Date;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.eclipse.kura.KuraException;
import org.eclipse.kura.cloud.CloudClient;
import org.eclipse.kura.cloud.CloudClientListener;
import org.eclipse.kura.cloud.CloudService;
import org.eclipse.kura.configuration.ConfigurableComponent;
import org.eclipse.kura.message.KuraPayload;
import org.json.JSONObject;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.ComponentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Dht11Service implements ConfigurableComponent, CloudClientListener  
{	
	private static final Logger s_logger = LoggerFactory.getLogger(Dht11Service.class);
	
	// Cloud Application identifier
	private static final String APP_ID = "DHT11_SERVICE";

	// Publishing Property Names
	private static final String   PUBLISH_RATE_PROP_NAME   = "publish.rate";
	private static final String   PUBLISH_TOPIC_PROP_NAME  = "publish.appTopic";
	private static final String   PUBLISH_QOS_PROP_NAME    = "publish.qos";
	private static final String   PUBLISH_RETAIN_PROP_NAME = "publish.retain";
	
	private static final String TEMPERATURE_PROPERTY_NAME = "temperature.property";
	private static final String HUMIDITY_PROPERTY_NAME = "humidity.property";
	private CloudService m_cloudService;
	private CloudClient m_cloudClient;
	
	private ScheduledExecutorService    m_worker;
	private ScheduledFuture<?>          m_handle;
	
	private Map<String, Object> m_properties;
	private DHT11 dht;
	
	// ----------------------------------------------------------------
	//
	//   Dependencies
	//
	// ----------------------------------------------------------------
	
	public Dht11Service() 
	{
		super();
		m_worker = Executors.newSingleThreadScheduledExecutor();
	}

	public void setCloudService(CloudService cloudService) {
		m_cloudService = cloudService;
	}

	public void unsetCloudService(CloudService cloudService) {
		m_cloudService = null;
	}
	
		
	// ----------------------------------------------------------------
	//
	//   Activation APIs
	//
	// ----------------------------------------------------------------

	protected void activate(ComponentContext componentContext, Map<String,Object> properties) 
	{
		s_logger.info("Activating Dht11Service...");
		
		m_properties = properties;
		for (String s : properties.keySet()) {
			s_logger.info("Activate - "+s+": "+properties.get(s));
		}
		try {
			if (dht == null)
				dht = new DHT11();
		} catch (Exception ex) {
			s_logger.error("Failed to init DHT11 sensor! " + ex.getMessage());
		}
		// get the mqtt client for this application
		try  {
			
			// Acquire a Cloud Application Client for this Application 
			s_logger.info("Getting CloudApplicationClient for {}...", APP_ID);
			m_cloudClient = m_cloudService.newCloudClient(APP_ID);
			m_cloudClient.addCloudClientListener(this);
			
			// Don't subscribe because these are handled by the default 
			// subscriptions and we don't want to get messages twice			
			doUpdate();
		}
		catch (Exception e) {
			s_logger.error("Error during component activation", e);
			throw new ComponentException(e);
		}
		s_logger.info("Activating Dht11Service... Done.");
	}
	
	protected void deactivate(ComponentContext componentContext) 
	{
		s_logger.debug("Deactivating Dht11Service...");

		// shutting down the worker and cleaning up the properties
		m_worker.shutdown();
		
		// Releasing the CloudApplicationClient
		s_logger.info("Releasing CloudApplicationClient for {}...", APP_ID);
		m_cloudClient.release();

		s_logger.debug("Deactivating Dht11Service... Done.");
	}	
	
	public void updated(Map<String,Object> properties)
	{
		s_logger.info("Updated Dht11Service...");

		// store the properties received
		m_properties = properties;
		for (String s : properties.keySet()) {
			s_logger.info("Update - "+s+": "+properties.get(s));
		}
		
		// try to kick off a new job
		doUpdate();
		s_logger.info("Updated Dht11Service... Done.");
	}
	
	
	// ----------------------------------------------------------------
	//
	//   Cloud Application Callback Methods
	//
	// ----------------------------------------------------------------
	
	@Override
	public void onConnectionEstablished() 
	{
		s_logger.info("Connection established");
		
		try {
			// Getting the lists of unpublished messages
			s_logger.info("Number of unpublished messages: {}", m_cloudClient.getUnpublishedMessageIds().size());
		} catch (KuraException e) {
			s_logger.error("Cannot get the list of unpublished messages");
		}
		
		try {
			// Getting the lists of in-flight messages
			s_logger.info("Number of in-flight messages: {}", m_cloudClient.getInFlightMessageIds().size());
		} catch (KuraException e) {
			s_logger.error("Cannot get the list of in-flight messages");
		}
		
		try {
			// Getting the lists of dropped in-flight messages
			s_logger.info("Number of dropped in-flight messages: {}", m_cloudClient.getDroppedInFlightMessageIds().size());
		} catch (KuraException e) {
			s_logger.error("Cannot get the list of dropped in-flight messages");
		}
	}

	@Override
	public void onConnectionLost() 
	{
		s_logger.warn("Connection lost!");
	}

	@Override
	public void onControlMessageArrived(String deviceId, String appTopic,
			KuraPayload msg, int qos, boolean retain) {
		s_logger.info("Control message arrived on assetId: {} and semantic topic: {}", deviceId, appTopic);
	}

	@Override
	public void onMessageArrived(String deviceId, String appTopic,
			KuraPayload msg, int qos, boolean retain) {
		s_logger.info("Message arrived on assetId: {} and semantic topic: {}", deviceId, appTopic);
	}

	@Override
	public void onMessagePublished(int messageId, String appTopic) {
		s_logger.info("Published message with ID: {} on application topic: {}", messageId, appTopic);
	}

	@Override
	public void onMessageConfirmed(int messageId, String appTopic) {
		s_logger.info("Confirmed message with ID: {} on application topic: {}", messageId, appTopic);
	}
	
	// ----------------------------------------------------------------
	//
	//   Private Methods
	//
	// ----------------------------------------------------------------

	/**
	 * Called after a new set of properties has been configured on the service
	 */
	private void doUpdate() 
	{
		// cancel a current worker handle if one if active
		if (m_handle != null) {
			m_handle.cancel(true);
		}
		
		if (!m_properties.containsKey(PUBLISH_RATE_PROP_NAME)) {
			s_logger.info("Update Dht11Service - Ignore as properties do not contain PUBLISH_RATE_PROP_NAME.");
			return;
		}
		
		// schedule a new worker based on the properties of the service
		int pubrate = (Integer) m_properties.get(PUBLISH_RATE_PROP_NAME);
		m_handle = m_worker.scheduleAtFixedRate(new Runnable() {		
			@Override
			public void run() {
				doPublish();
			}
		}, 0, pubrate, TimeUnit.MILLISECONDS);
	}
	
	
	/**
	 * Called at the configured rate to publish the next temperature measurement.
	 */
	private void doPublish() 
	{				
		
		
		//get temperature from the DHT11 sensor
		SensorData data = new SensorData();
		try {
			data = dht.getTemperature();
			s_logger.info("Current data is: " + data.toString());
		} catch (Exception ex) {
			s_logger.error("Failed to read temperature from DHT11: " + ex.getMessage());
		}

		String humidityKey = (String) m_properties.get(HUMIDITY_PROPERTY_NAME);
		String temperatureKey = (String) m_properties.get(TEMPERATURE_PROPERTY_NAME);
		// Publish the message
		try {
			JSONObject joTemp = new JSONObject();
			joTemp.put(temperatureKey, data.getTemperature());
			publishMessage(joTemp);
			JSONObject joHum = new JSONObject();
			joHum.put(humidityKey, data.getHumidity());
			publishMessage(joHum);
		} 
		catch (Exception e) {
			s_logger.error("Cannot wrap data to json: " + data + "\n" + e.toString());
		}
	}
	private void publishMessage(JSONObject json) {
		// fetch the publishing configuration from the publishing properties
		String  topic  = (String) m_properties.get(PUBLISH_TOPIC_PROP_NAME);
		Integer qos    = (Integer) m_properties.get(PUBLISH_QOS_PROP_NAME);
		Boolean retain = (Boolean) m_properties.get(PUBLISH_RETAIN_PROP_NAME);
		try{
			int messageId = m_cloudClient.publish(topic, json.toString().getBytes(), qos, retain, 5);
	
			//int messageId = m_cloudClient.publish(topic, payload, qos, retain);
			s_logger.info("Published to {} message: {} with ID: {}", new Object[] {topic, json.toString(), messageId});
		} 
		catch (Exception e) {
			s_logger.error("Cannot publish topic: "+topic, e);
		}
	}
}

