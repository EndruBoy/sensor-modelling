package org.eclipse.kura.pi4j.dht11;

public class SensorData {
	private String temperature = "";
	private String humidity = "";
	
	@Override
	public String toString() {
		return "SensorData [temperature=" + temperature + ", humidity=" + humidity + "]";
	}
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public void setTemperature(float temperature) {
		this.temperature = String.valueOf(temperature);
	}
	public String getHumidity() {
		return humidity;
	}
	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}
	public void setHumidity(float humidity) {
		this.humidity = String.valueOf(humidity);
	}
	
}
